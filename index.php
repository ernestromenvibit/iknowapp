<?php


class connection{

private $conn;
private $servername;
private $username;
private $dbname;
private $password;
public $clients;

// Create connection
public function __construct(){
$this->servername = "localhost";
$this->username = "root";
$this->dbname = "testingdata";
$this->password = "";

    try {
        $this->conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, $this->password);
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


    } catch(PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }
}


public function select(){
    $sql = "SELECT first_name FROM clients";

    $stmt =  $this->conn->prepare($sql);
    $stmt->execute();
    $this->clients = $stmt->fetchAll();
// var_dump($this->clients,'inside select');
return $this->clients;
}

}

 $ob = new connection();
$result = $ob->select();
echo '<pre>';
// var_dump($result,'result inside object');
echo '</pre>';
?>



<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>הכסף שלך</title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">

    <!-- Bootstrap Core CSS -->
    <link href="./public/admin/css/rtl/bootstrap.min.css" rel="stylesheet">

    <!-- not use this in ltr -->
    <link href="./public/admin/css/rtl/bootstrap.rtl.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="./public/admin/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="./public/admin/css/plugins/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="./public/admin/css/rtl/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="./public/admin/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="./public/admin/css/font-awesome/font-awesome.min.css" rel="stylesheet" type="text/css">


    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>

    <link rel="stylesheet" href="./public/css/admin-app.css">
    <link rel="stylesheet" href="https://printjs-4de6.kxcdn.com/print.min.css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.3/datatables.min.css" />

    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.3/datatables.min.js" defer></script>

    <!-- toast css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css"/>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'header.php'; ?>
        <div id="page-wrapper">
            <!-- /.row -->
            <div class="dropdown">
                <button class="btn btn-primary btn-sm form-control dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Dashboard Navigations
                </button>
                <div class="dropdown-menu" aria-labelledby="triggerId">
                    <div style="padding-top: 5px;" class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <i class="fas fa-users fa-3x"></i>
                                        </div>
                                        <div class="col-xs-8 text-right">
                                            <div class="huge">26</div>
                                            <div>System Users</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="manage-users">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-green">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <i class="fa fa-tasks fa-3x"></i>
                                        </div>
                                        <div class="col-xs-8 text-right">
                                            <div class="huge">12</div>
                                            <div>Har Data Requests!</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="manage-har-data-requests">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-yellow">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <i class="fas fa-book-reader fa-3x"></i>
                                        </div>
                                        <div class="col-xs-8 text-right">
                                            <div class="huge">124</div>
                                            <div>Har Data Reports</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="manage-har-data-reports">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-red">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <i class="fas fa-user-circle fa-3x"></i>
                                        </div>
                                        <div class="col-xs-8 text-right">
                                            <div class="huge">13</div>
                                            <div>User Profile!</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="view-profile">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div style="padding-top: 5px;" class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Har Data Requests
                            <div class="pull-left">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Actions
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu" dir="rtl">
                                        <li>
                                            <a class="btn btn-sm" href="#agent-login-to-gov-system" id="agent-login-to-gov-system" onclick="agent_login_to_government_system()">Login As Agent</a>
                                        </li>
                                        <li>
                                            <a class="btn btn-sm" href="#view-har-data-requests" id="view-har-data-requests-btn">View Fullscreen</a>
                                        </li>
                                        <li>
                                            <a class="btn btn-sm" href="#refresh-har-data-request-data" id="refresh-har-data-request-data" onclick="refresh_har_request_data()">Refresh</a>
                                        </li>
                                        <li>
                                            <a class="btn btn-sm" href="#print-client-requests" id="export" onclick="download_table_as_csv('get_har_data_table', separator = ',')">Save</a>
                                        </li>
                                        <li>
                                            <a class="btn btn-sm" href="#print-client-requests" id="print-client-har-requests" onclick="printJS({ printable: 'har-request-table-printable', type: 'html', header: `<center><h1>HAR REQUESTS REPORT</h1></center`});">Print</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="morris-area-chart">
                                <!-- Requests Table -->
                                <div class="request-waiting-spinner" id="request-waiting-spinner">
                                <!-- id="get_har_data_table" -->
</div>
<div class="table-responsive" id="har-request-table-printable">
    <table id="myTable" class="table table-hover table table-striped table-bordered har-requests-table" cellpadding="0" cellspacing="0" dir="RTL" width="100%" style="border-collapse: collapse; width: 100%; border-spacing: 0px;">
        <thead>
            <tr>
                <td style="text-transform: uppercase" align="center" dir="rtl">Actions</td>
                <td style="text-transform: uppercase" align="center" dir="rtl">
                    בחר
                    <input type="checkbox" class="form-check-input har_data_request_select_all_checkbox" name="" id="select-all" value="checkedValue">
                </td>
                <td style="text-transform: uppercase" align="center" dir="rtl">ת"ז</td>
                <td style="text-transform: uppercase" align="center" dir="rtl">כותברת כלשהי</td>
                <td style="text-transform: uppercase" align="center" dir="rtl">CHILDREN ID's</td>
                <td style="text-transform: uppercase" align="center" dir="rtl">PARENT ID</td>
                <td style="text-transform: uppercase" align="center" dir="rtl">PARTNER ID</td>
                <td style="text-transform: uppercase" align="center" dir="rtl">תאריך יצירה</td>
                <td style="text-transform: uppercase" align="center" dir="rtl">שם פרטי</td>
                <td style="text-transform: uppercase" align="center" dir="rtl">שם משפחה</td>
                <td style="text-transform: uppercase" align="center" dir="rtl">Age</td>
                <td style="text-transform: uppercase" align="center" dir="rtl">טלפון</td>
                <td style="text-transform: uppercase" align="center" dir="rtl">Email</td>
                <td style="text-transform: uppercase" align="center" dir="rtl">ISSUE DATE</td>
                <td style="text-transform: uppercase" align="center" dir="rtl">IP Address</td>
                <td style="text-transform: uppercase" align="center" dir="rtl">טלפון OTP</td>
                <td style="text-transform: uppercase" align="center" dir="rtl">טלפון שותף</td>
                <td style="text-transform: uppercase" align="center" dir="rtl">נשלח לבן זוג</td> 
                <td style="text-transform: uppercase" align="center" dir="rtl">קישור תיקיה עם קבצים</td>
                <td style="text-transform: uppercase" align="center" dir="rtl">REPORT</td>
                <td style="text-transform: uppercase" align="center" dir="rtl">recommendation exist</td>
                <td style="text-transform: uppercase" align="center" dir="rtl">הערות</td>
            </tr>
        </thead>
        <?php for($x=0; $x < count($result); ++$x):?>
        <tbody class="har_data_tbody" id="har_data_tbody">

        <td style="text-transform: uppercase" align="center" dir="rtl"><button class="btn btn-secondary dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Actions
                                </button></td>
        <td style="text-transform: uppercase" align="center" dir="rtl"><input type="checkbox"/></td>
        <td style="text-transform: uppercase" align="center" dir="rtl"><?php

        echo ($result[$x]['first_name']);
        ?></td>
        <td style="text-transform: uppercase" align="center" dir="rtl">DID NOT CREATE HAR REQUEST WITHOUT PENDING MISLAKA</td>
        <td style="text-transform: uppercase" align="center" dir="rtl">335024519</td>
        <td style="text-transform: uppercase" align="center" dir="rtl">	GOT HAR REPORT WITHOUT PENDING MISLAKA</td>
        <td style="text-transform: uppercase" align="center" dir="rtl">337418388</td>
        <td style="text-transform: uppercase" align="center" dir="rtl">032256562</td>
        <td style="text-transform: uppercase" align="center" dir="rtl">034212167</td>
        <td style="text-transform: uppercase" align="center" dir="rtl">2022-04-28 10:01:26</td>
        <td style="text-transform: uppercase" align="center" dir="rtl">נחמיאס</td>
        <td style="text-transform: uppercase" align="center" dir="rtl">0549329876</td>
        <td style="text-transform: uppercase" align="center" dir="rtl"><a href="#">MegiddoGuy@Gmail.com</a></td>
        <td style="text-transform: uppercase" align="center" dir="rtl">2007-01-03</td>
        <td style="text-transform: uppercase" align="center" dir="rtl">79.183.44.176</td>
        <td style="text-transform: uppercase" align="center" dir="rtl">0549329876</td>
        <td style="text-transform: uppercase" align="center" dir="rtl">054998769</td>
        <td style="text-transform: uppercase" align="center" dir="rtl">NO</td>
        <td style="text-transform: uppercase" align="center" dir="rtl"><a href="#">FOLDER</a></td>
        <td style="text-transform: uppercase" align="center" dir="rtl"><a href="#">לינק כלשהו</a></td>
        <td style="text-transform: uppercase" align="center" dir="rtl">N/A</td>
        <td style="text-transform: uppercase" align="center" dir="rtl">
    <textarea>NO COMMENT</textarea>
    </td>
        </tbody>
        <?php endfor;?>

    </table>
</div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

            <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script> -->

        <!-- toast -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="./public/js/admin-app.js"></script>
        <script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

        <!-- jQuery Version 1.11.0 -->
        <script src="./public/admin/js/jquery-1.11.0.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="./public/admin/js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="./public/admin/js/metisMenu/metisMenu.min.js"></script>

        <!-- Morris Charts JavaScript -->
        <script src="./public/admin/js/raphael/raphael.min.js"></script>
        <script src="./public/admin/js/morris/morris.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="./public/admin/js/sb-admin-2.js"></script> 
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>

        <script>
$(document).ready( function () {
    $('#myTable').DataTable();
} );

</script>
        </body>

        </html>




