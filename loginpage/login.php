
<!DOCTYPE html>
<html lang="he" dir='RTL'>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>הכסף שלך</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <link rel="stylesheet" href="./public/css/admin-app.css">
    <link rel="stylesheet" href="https://printjs-4de6.kxcdn.com/print.min.css">


    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.3/datatables.min.css" />

    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.3/datatables.min.js" defer></script>
</head>

<body>
    <main class="step_one" id="step_one">
        <div style="height: 100vh;" class="card w-100">
            <div class="card-body">
                <div class="w-75">
                    <center>
                        <div style="padding-top: 8rem; padding-bottom: 7rem" class="logo-img-container">
                            <img src="./public/images/LOGO.jpg" alt="" height="100" width="400">
                        </div>
                    </center>
                </div>
                <div class="login_form_container">
                    <form action="">
                        <div class="login-status-container w-75" id="login-status-container">
                            <p align="right">
                                                        </p>
                        </div>
                        <div class="">
                            <p align="right"><span dir="rtl">
                                    <font face="Tahoma">
                                        <label><strong>Email/Username:</strong></label>
                                        <input required="required" type="text" name="username" id="username" class="form-control w-75" placeholder="" aria-describedby="helpId" autocomplete="on">
                                    </font>
                                </span>
                        </div>
                        <div class="">
                            <p align="right"><span dir="rtl">
                                    <font face="Tahoma">
                                        <label for=""><strong>Password:</strong></label>
                                        &nbsp; <input required="required" type="password" name="password" id="password" class="form-control w-75" placeholder="" aria-describedby="helpId" autocomplete="on">
                                    </font>
                                </span>
                        </div>

                        <div class="">
                            <p align="right"><span dir="rtl">
                                    <font face="Tahoma">

                                        <input style="border-radius: 24px" type="button" name="login-submit-btn" id="login-submit-btn" class="btn btn-primary w-75 form-control text-center submit_info" aria-describedby="helpId" value="Login">
                                    </font>
                                </span>
                        </div>

                        <div class="w-75">
                            <div class="form-check form-check-inline">
                                <p align="right" style="align: right;">
                                    <span dir="rtl">
                                        <label class="form-check-label">
                                            <font face="Tahoma">
                                                &nbsp; Don't have an account? Click <a href="/register" id="register-link" class="pl-5 register-link">here!</a>
                                            </font>
                                        </label>
                                    </span>
                                </p>
                                <p align="right" style="align: right;">
                                    <span dir="rtl">
                                        <label class="form-check-label">
                                            <font face="Tahoma">
                                                &nbsp; Are you a client? Click <a target="_blank" href="/" id="go-client-side-navigation-link" class="pl-5 go-client-side-navigation-link">here!</a>
                                            </font>
                                        </label>
                                    </span>
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>


            <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script> -->

        <!-- toast -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="./public/js/admin-app.js"></script>
        <script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

        <!-- jQuery Version 1.11.0 -->
        <script src="./public/admin/js/jquery-1.11.0.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="./public/admin/js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="./public/admin/js/metisMenu/metisMenu.min.js"></script>

        <!-- Morris Charts JavaScript -->
        <script src="./public/admin/js/raphael/raphael.min.js"></script>
        <script src="./public/admin/js/morris/morris.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="./public/admin/js/sb-admin-2.js"></script> 
        </body>

        </html>