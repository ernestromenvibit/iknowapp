-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 12, 2022 at 01:22 PM
-- Server version: 10.3.32-MariaDB
-- PHP Version: 7.3.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `daphascomp_polywizz_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `partner_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(80) DEFAULT NULL,
  `phone` varchar(20) NOT NULL,
  `id_number` varchar(20) NOT NULL,
  `parent_national_id_number` varchar(255) DEFAULT NULL,
  `id_cert_issuance_date` date NOT NULL,
  `age` int(11) DEFAULT NULL,
  `family` varchar(255) DEFAULT NULL,
  `wife_telephone_number` varchar(11) DEFAULT NULL,
  `partner_national_id_number` varchar(255) DEFAULT NULL,
  `client_comment` longtext DEFAULT NULL,
  `har_data_excel_file` varchar(255) DEFAULT NULL,
  `har_data_excel_file_landing_pg_link` varchar(255) DEFAULT NULL,
  `mislaka_files` varchar(255) DEFAULT NULL,
  `mislaka_report_files_status` int(11) DEFAULT NULL,
  `signed_mislaka` varchar(5) DEFAULT NULL,
  `terms_and_conditions` varchar(11) NOT NULL,
  `client_id` longtext DEFAULT NULL COMMENT 'client id generated after crm client create success',
  `otp_code` varchar(4) DEFAULT NULL,
  `recommedations` int(11) DEFAULT NULL,
  `har_report_request_error` longtext DEFAULT NULL,
  `mislaka_report_request_error` longtext DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT '1=>create_client_incomplete, 2=> create_client_complete, 3=>get_har_data_incomplete, 4=>get_har_data_complete\r\n',
  `ip_address` text DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `partner_id`, `first_name`, `last_name`, `email`, `phone`, `id_number`, `parent_national_id_number`, `id_cert_issuance_date`, `age`, `family`, `wife_telephone_number`, `partner_national_id_number`, `client_comment`, `har_data_excel_file`, `har_data_excel_file_landing_pg_link`, `mislaka_files`, `mislaka_report_files_status`, `signed_mislaka`, `terms_and_conditions`, `client_id`, `otp_code`, `recommedations`, `har_report_request_error`, `mislaka_report_request_error`, `status`, `ip_address`, `created_at`, `updated_at`) VALUES
(1, 0, 'TEST', '01', '', '0549329876', '032256562', NULL, '2007-01-03', 333, 'NULL', NULL, '', NULL, '7ec3a.xlsx', NULL, NULL, 0, NULL, 'accepted', '', '8876', 1, '', NULL, 3, '77.137.67.73', '2022-01-12 18:01:24', '2022-01-12 18:06:27'),
(2, 0, 'WRONG', 'CRED', 'ppp', '0549329876', '2', NULL, '2022-01-12', 2, 'NULL', NULL, '', NULL, '', NULL, NULL, 0, NULL, 'accepted', '', '3370', 0, 'The system experienced some problems  Error - got logged out of login gov il  re-login using   login to gov il route or wait a few minutes and try again ', NULL, 0, '77.137.67.73', '2022-01-12 18:10:04', '2022-01-12 20:01:11'),
(3, 0, 'TEST', '05', 'null', '0549329876', 'd', NULL, '2022-01-12', 4, 'NULL', NULL, '', NULL, '0dfad.xlsx', NULL, NULL, 0, NULL, 'accepted', '', '7502', 0, '', NULL, 3, '77.137.67.73', '2022-01-12 18:14:44', '2022-01-12 19:45:49'),
(4, 0, 'Sigal', 'Hershko', 'gicehajunior76@gmail.com', '0549329876', '029489374', NULL, '2009-04-23', 89, 'NULL', NULL, '', NULL, '76f61.xlsx', NULL, NULL, 0, NULL, 'accepted', '', '3718', 1, '', NULL, 3, '154.159.238.15', '2022-01-12 19:47:56', '2022-01-12 19:49:14'),
(5, 0, 'w', 'w', 'null', '0549329876', '2222', NULL, '2022-01-12', 22, 'NULL', NULL, '', NULL, 'e3990.xlsx', NULL, NULL, 0, NULL, 'accepted', '', '3278', 1, '', NULL, 3, '77.137.67.73', '2022-01-12 19:57:26', '2022-01-12 19:58:36');

-- --------------------------------------------------------

--
-- Table structure for table `get_har_data_requests`
--

CREATE TABLE `get_har_data_requests` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `get_status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `get_har_data_requests`
--

INSERT INTO `get_har_data_requests` (`id`, `client_id`, `get_status`, `created_at`, `updated_at`) VALUES
(1, 780008, 1, '2021-12-26 20:09:45', '2021-12-26 20:09:45'),
(2, 780008, 1, '2021-12-26 20:09:49', '2021-12-26 20:09:49'),
(3, 780015, 1, '2021-12-26 20:25:55', '2021-12-26 20:25:55'),
(4, 780015, 1, '2021-12-26 20:25:59', '2021-12-26 20:25:59'),
(5, 780018, 1, '2021-12-26 20:32:49', '2021-12-26 20:32:49'),
(6, 780018, 1, '2021-12-26 20:32:53', '2021-12-26 20:32:53'),
(7, 780029, 1, '2021-12-26 20:44:09', '2021-12-26 20:44:09'),
(8, 780029, 1, '2021-12-26 20:44:13', '2021-12-26 20:44:13'),
(9, 780097, 0, '2021-12-27 00:23:48', '2021-12-27 00:23:48'),
(10, 780148, 0, '2021-12-27 09:37:35', '2021-12-27 09:37:35'),
(11, 780193, 0, '2021-12-27 10:19:50', '2021-12-27 10:19:50'),
(12, 780213, 1, '2021-12-27 10:23:50', '2021-12-27 10:23:50'),
(13, 780213, 1, '2021-12-27 10:23:53', '2021-12-27 10:23:53'),
(14, 781129, 1, '2021-12-27 15:32:45', '2021-12-27 15:32:45'),
(15, 781409, 1, '2021-12-27 18:02:08', '2021-12-27 18:02:08'),
(16, 781409, 1, '2021-12-27 18:02:08', '2021-12-27 18:02:08'),
(17, 781409, 1, '2021-12-27 18:02:08', '2021-12-27 18:02:08'),
(18, 781409, 1, '2021-12-27 18:02:08', '2021-12-27 18:02:08'),
(19, 781421, 1, '2021-12-27 18:10:42', '2021-12-27 18:10:42'),
(20, 781436, 1, '2021-12-27 18:26:05', '2021-12-27 18:26:05'),
(21, 781526, 0, '2021-12-27 21:05:17', '2021-12-27 21:05:17'),
(22, 781535, 0, '2021-12-27 21:30:00', '2021-12-27 21:30:00'),
(23, 781535, 0, '2021-12-27 21:33:01', '2021-12-27 21:33:01'),
(24, 781535, 0, '2021-12-27 21:33:18', '2021-12-27 21:33:18'),
(25, 781575, 0, '2021-12-27 23:06:26', '2021-12-27 23:06:26'),
(26, 781575, 0, '2021-12-27 23:09:49', '2021-12-27 23:09:49'),
(27, 781575, 0, '2021-12-27 23:11:27', '2021-12-27 23:11:27'),
(28, 781585, 0, '2021-12-27 23:39:48', '2021-12-27 23:39:48'),
(29, 781588, 1, '2021-12-27 23:53:48', '2021-12-27 23:53:48'),
(30, 781588, 1, '2021-12-27 23:53:52', '2021-12-27 23:53:52'),
(31, 781998, 1, '2021-12-28 12:16:18', '2021-12-28 12:16:18'),
(32, 781998, 1, '2021-12-28 12:16:22', '2021-12-28 12:16:22'),
(33, 781998, 1, '2021-12-28 12:16:26', '2021-12-28 12:16:26'),
(34, 781998, 1, '2021-12-28 12:16:30', '2021-12-28 12:16:30'),
(35, 781998, 1, '2021-12-28 12:16:34', '2021-12-28 12:16:34'),
(36, 781998, 1, '2021-12-28 12:16:38', '2021-12-28 12:16:38'),
(37, 781998, 1, '2021-12-28 12:16:42', '2021-12-28 12:16:42'),
(38, 781998, 1, '2021-12-28 12:16:46', '2021-12-28 12:16:46'),
(39, 781998, 1, '2021-12-28 12:16:50', '2021-12-28 12:16:50'),
(40, 781998, 1, '2021-12-28 12:16:54', '2021-12-28 12:16:54'),
(41, 781998, 1, '2021-12-28 12:16:58', '2021-12-28 12:16:58'),
(42, 781998, 1, '2021-12-28 12:17:02', '2021-12-28 12:17:02'),
(43, 781998, 1, '2021-12-28 12:17:06', '2021-12-28 12:17:06'),
(44, 781998, 1, '2021-12-28 12:17:10', '2021-12-28 12:17:10'),
(45, 781998, 1, '2021-12-28 12:17:14', '2021-12-28 12:17:14'),
(46, 781998, 1, '2021-12-28 12:17:18', '2021-12-28 12:17:18'),
(47, 781998, 1, '2021-12-28 12:17:22', '2021-12-28 12:17:22'),
(48, 781998, 1, '2021-12-28 12:17:42', '2021-12-28 12:17:42'),
(49, 781998, 1, '2021-12-28 12:17:42', '2021-12-28 12:17:42'),
(50, 781998, 1, '2021-12-28 12:17:42', '2021-12-28 12:17:42'),
(51, 781998, 1, '2021-12-28 12:17:42', '2021-12-28 12:17:42'),
(52, 782163, 1, '2021-12-28 13:08:24', '2021-12-28 13:08:24'),
(53, 782617, 0, '2021-12-28 16:00:09', '2021-12-28 16:00:09'),
(54, 782720, 0, '2021-12-28 16:45:45', '2021-12-28 16:45:45'),
(55, 782742, 0, '2021-12-28 16:53:19', '2021-12-28 16:53:19'),
(56, 782742, 1, '2021-12-28 16:55:56', '2021-12-28 16:55:56'),
(57, 782762, 0, '2021-12-28 17:09:40', '2021-12-28 17:09:40'),
(58, 782883, 0, '2021-12-28 18:22:52', '2021-12-28 18:22:52'),
(59, 782886, 1, '2021-12-28 18:26:38', '2021-12-28 18:26:38'),
(60, 782886, 1, '2021-12-28 18:26:38', '2021-12-28 18:26:38'),
(61, 782886, 1, '2021-12-28 18:26:38', '2021-12-28 18:26:38'),
(62, 782886, 1, '2021-12-28 18:26:40', '2021-12-28 18:26:40'),
(63, 782886, 1, '2021-12-28 18:26:42', '2021-12-28 18:26:42'),
(64, 782886, 1, '2021-12-28 18:26:43', '2021-12-28 18:26:43'),
(65, 782886, 1, '2021-12-28 18:26:43', '2021-12-28 18:26:43'),
(66, 782886, 1, '2021-12-28 18:26:43', '2021-12-28 18:26:43'),
(67, 782886, 1, '2021-12-28 18:26:44', '2021-12-28 18:26:44'),
(68, 782886, 1, '2021-12-28 18:26:46', '2021-12-28 18:26:46'),
(69, 782886, 1, '2021-12-28 18:26:46', '2021-12-28 18:26:46'),
(70, 782886, 1, '2021-12-28 18:26:46', '2021-12-28 18:26:46'),
(71, 782886, 1, '2021-12-28 18:26:46', '2021-12-28 18:26:46'),
(72, 782886, 1, '2021-12-28 18:26:46', '2021-12-28 18:26:46'),
(73, 782931, 0, '2021-12-28 19:34:55', '2021-12-28 19:34:55'),
(74, 783025, 0, '2021-12-29 00:04:29', '2021-12-29 00:04:29'),
(75, 783026, 1, '2021-12-29 00:07:41', '2021-12-29 00:07:41'),
(76, 783026, 1, '2021-12-29 00:07:41', '2021-12-29 00:07:41'),
(77, 783026, 1, '2021-12-29 00:07:44', '2021-12-29 00:07:44'),
(78, 783064, 1, '2021-12-29 09:41:09', '2021-12-29 09:41:09'),
(79, 783064, 1, '2021-12-29 09:41:13', '2021-12-29 09:41:13'),
(80, 784032, 0, '2021-12-29 15:35:00', '2021-12-29 15:35:00'),
(81, 784063, 0, '2021-12-29 16:13:13', '2021-12-29 16:13:13'),
(82, 0, 3, '2022-01-03 20:45:42', '2022-01-03 20:45:42'),
(83, 0, 3, '2022-01-03 21:34:11', '2022-01-03 21:34:11'),
(84, 0, 3, '2022-01-04 21:53:11', '2022-01-04 21:53:11'),
(85, 0, 3, '2022-01-05 09:46:10', '2022-01-05 09:46:10'),
(86, 0, 3, '2022-01-05 18:55:08', '2022-01-05 18:55:08'),
(87, 0, 3, '2022-01-05 19:07:55', '2022-01-05 19:07:55'),
(88, 0, 3, '2022-01-05 19:17:37', '2022-01-05 19:17:37'),
(89, 0, 3, '2022-01-05 19:26:34', '2022-01-05 19:26:34'),
(90, 0, 3, '2022-01-05 19:27:15', '2022-01-05 19:27:15'),
(91, 0, 3, '2022-01-05 19:37:33', '2022-01-05 19:37:33'),
(92, 0, 3, '2022-01-05 19:38:13', '2022-01-05 19:38:13'),
(93, 0, 3, '2022-01-05 19:41:28', '2022-01-05 19:41:28'),
(94, 0, 3, '2022-01-05 19:41:54', '2022-01-05 19:41:54'),
(95, 0, 3, '2022-01-05 19:53:00', '2022-01-05 19:53:00'),
(96, 0, 3, '2022-01-06 11:54:01', '2022-01-06 11:54:01'),
(97, 0, 3, '2022-01-06 11:55:28', '2022-01-06 11:55:28'),
(98, 0, 3, '2022-01-06 11:58:31', '2022-01-06 11:58:31'),
(99, 0, 3, '2022-01-06 11:59:22', '2022-01-06 11:59:22'),
(100, 0, 3, '2022-01-06 11:59:33', '2022-01-06 11:59:33'),
(101, 0, 3, '2022-01-06 12:38:57', '2022-01-06 12:38:57'),
(102, 0, 3, '2022-01-06 12:40:21', '2022-01-06 12:40:21'),
(103, 0, 3, '2022-01-10 09:41:49', '2022-01-10 09:41:49'),
(104, 0, 3, '2022-01-10 11:34:10', '2022-01-10 11:34:10'),
(105, 0, 3, '2022-01-10 12:05:24', '2022-01-10 12:05:24'),
(106, 0, 3, '2022-01-10 13:22:57', '2022-01-10 13:22:57'),
(107, 0, 3, '2022-01-10 14:29:13', '2022-01-10 14:29:13'),
(108, 0, 3, '2022-01-10 15:51:37', '2022-01-10 15:51:37'),
(109, 0, 3, '2022-01-10 16:33:13', '2022-01-10 16:33:13'),
(110, 0, 3, '2022-01-10 17:30:43', '2022-01-10 17:30:43'),
(111, 0, 3, '2022-01-11 15:30:17', '2022-01-11 15:30:17'),
(112, 0, 3, '2022-01-11 15:30:57', '2022-01-11 15:30:57'),
(113, 0, 3, '2022-01-11 15:31:54', '2022-01-11 15:31:54'),
(114, 0, 3, '2022-01-11 15:38:43', '2022-01-11 15:38:43'),
(115, 0, 3, '2022-01-11 15:42:21', '2022-01-11 15:42:21'),
(116, 0, 3, '2022-01-11 15:49:05', '2022-01-11 15:49:05'),
(117, 0, 3, '2022-01-11 16:31:55', '2022-01-11 16:31:55'),
(118, 0, 3, '2022-01-11 16:32:38', '2022-01-11 16:32:38'),
(119, 0, 3, '2022-01-11 16:49:11', '2022-01-11 16:49:11'),
(120, 0, 3, '2022-01-11 17:17:44', '2022-01-11 17:17:44'),
(121, 0, 3, '2022-01-11 18:42:56', '2022-01-11 18:42:56'),
(122, 0, 3, '2022-01-11 19:08:48', '2022-01-11 19:08:48'),
(123, 0, 3, '2022-01-11 23:37:32', '2022-01-11 23:37:32'),
(124, 0, 3, '2022-01-12 07:30:55', '2022-01-12 07:30:55'),
(125, 0, 3, '2022-01-12 07:37:03', '2022-01-12 07:37:03'),
(126, 0, 3, '2022-01-12 07:40:41', '2022-01-12 07:40:41'),
(127, 0, 3, '2022-01-12 13:40:53', '2022-01-12 13:40:53'),
(128, 0, 3, '2022-01-12 13:59:27', '2022-01-12 13:59:27'),
(129, 0, 3, '2022-01-12 14:01:16', '2022-01-12 14:01:16'),
(130, 0, 3, '2022-01-12 14:06:22', '2022-01-12 14:06:22'),
(131, 0, 3, '2022-01-12 14:10:02', '2022-01-12 14:10:02'),
(132, 0, 3, '2022-01-12 14:30:49', '2022-01-12 14:30:49'),
(133, 0, 3, '2022-01-12 14:32:57', '2022-01-12 14:32:57'),
(134, 0, 3, '2022-01-12 14:37:50', '2022-01-12 14:37:50'),
(135, 0, 3, '2022-01-12 15:17:39', '2022-01-12 15:17:39'),
(136, 0, 3, '2022-01-12 15:38:41', '2022-01-12 15:38:41'),
(137, 0, 3, '2022-01-12 15:45:38', '2022-01-12 15:45:38'),
(138, 0, 3, '2022-01-12 15:51:39', '2022-01-12 15:51:39'),
(139, 0, 3, '2022-01-12 16:00:02', '2022-01-12 16:00:02'),
(140, 0, 3, '2022-01-12 16:28:37', '2022-01-12 16:28:37'),
(141, 0, 3, '2022-01-12 16:31:52', '2022-01-12 16:31:52'),
(142, 0, 3, '2022-01-12 17:20:46', '2022-01-12 17:20:46'),
(143, 0, 3, '2022-01-12 17:23:03', '2022-01-12 17:23:03'),
(144, 0, 3, '2022-01-12 17:28:28', '2022-01-12 17:28:28'),
(145, 0, 0, '2022-01-12 18:01:31', '2022-01-12 18:01:31'),
(146, 0, 3, '2022-01-12 18:05:59', '2022-01-12 18:05:59'),
(147, 0, 3, '2022-01-12 18:55:57', '2022-01-12 18:55:57'),
(148, 0, 0, '2022-01-12 19:00:28', '2022-01-12 19:00:28'),
(149, 0, 3, '2022-01-12 19:48:04', '2022-01-12 19:48:04'),
(150, 0, 3, '2022-01-12 19:57:30', '2022-01-12 19:57:30');

-- --------------------------------------------------------

--
-- Table structure for table `har_bituach_api_settings`
--

CREATE TABLE `har_bituach_api_settings` (
  `id` int(11) NOT NULL,
  `HAR_BITUACH_MAIN_URL` varchar(255) DEFAULT NULL,
  `HAR_BITUACH_LOGIN_ENDPOINT` varchar(255) DEFAULT NULL,
  `HAR_BITUACH_ENTER_OTP_ENDPOINT` varchar(255) DEFAULT NULL,
  `HAR_BITUACH_GET_INSURANCE_DATA_ENDPOINT` varchar(255) DEFAULT NULL,
  `HAR_BITUACH_REQUEST_MODE` varchar(255) DEFAULT NULL,
  `HAR_BITUACH_SMS_USERNAME` varchar(255) DEFAULT NULL,
  `HAR_BITUACH_SMS_PASSWORD` varchar(255) DEFAULT NULL,
  `HAR_BITUACH_AGENT_AGENT_PHONE` varchar(50) DEFAULT NULL,
  `HAR_BITUACH_AGENT_ID` varchar(255) DEFAULT NULL,
  `HAR_BITUACH_AGENT_GOV_PASSWORD` longtext DEFAULT NULL,
  `HAR_BITUACH_KILL_LOGIN_SESSION_ENDPOINT` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `har_bituach_api_settings`
--

INSERT INTO `har_bituach_api_settings` (`id`, `HAR_BITUACH_MAIN_URL`, `HAR_BITUACH_LOGIN_ENDPOINT`, `HAR_BITUACH_ENTER_OTP_ENDPOINT`, `HAR_BITUACH_GET_INSURANCE_DATA_ENDPOINT`, `HAR_BITUACH_REQUEST_MODE`, `HAR_BITUACH_SMS_USERNAME`, `HAR_BITUACH_SMS_PASSWORD`, `HAR_BITUACH_AGENT_AGENT_PHONE`, `HAR_BITUACH_AGENT_ID`, `HAR_BITUACH_AGENT_GOV_PASSWORD`, `HAR_BITUACH_KILL_LOGIN_SESSION_ENDPOINT`, `created_at`, `updated_at`) VALUES
(1, 'http://3.15.197.11:80', 'http://3.15.197.11:80/login_to_gov_il', 'http://3.15.197.11:80/enter_otp', 'http://3.15.197.11:80/get_insurance_data', 'auto', 'AC70df75cc641ee99e717cdf492fea22cc', '220305832cf9bae19354b160080fa979', '17813814678', '024325847', 'Sigi@120369', 'http://3.15.197.11:80/kill_session', '2021-12-22 13:46:57', '2022-01-10 11:03:26');

-- --------------------------------------------------------

--
-- Table structure for table `partials_settings`
--

CREATE TABLE `partials_settings` (
  `id` int(11) NOT NULL,
  `age` int(11) DEFAULT NULL,
  `premia` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partials_settings`
--

INSERT INTO `partials_settings` (`id`, `age`, `premia`, `created_at`, `updated_at`) VALUES
(1, 40, 300, '2021-12-28 20:57:46', '2022-01-06 06:15:16');

-- --------------------------------------------------------

--
-- Table structure for table `strings`
--

CREATE TABLE `strings` (
  `id` int(11) NOT NULL,
  `strings` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `strings`
--

INSERT INTO `strings` (`id`, `strings`, `created_at`, `updated_at`) VALUES
(3, '× ×™×ª×•×—×™× ×‘×—×•\"×œ', '2021-12-27 12:34:01', '2021-12-27 12:34:01'),
(4, '×›×ª×‘ ×©×™×¨×•×ª ×¨×¤×•××” ×ž×©×œ×™×ž×”', '2021-12-27 12:34:19', '2021-12-27 12:34:19'),
(5, '×ž×—×œ×•×ª ×§×©×•×ª', '2021-12-27 15:11:31', '2021-12-27 15:11:31'),
(6, '×‘×™×˜×•×— ×—×™×™× ×œ×ž×§×¨×” ×ž×•×•×ª', '2021-12-27 15:11:46', '2021-12-27 15:11:46'),
(7, '×”×©×ª×œ×•×ª', '2021-12-27 15:12:26', '2021-12-27 15:12:26'),
(8, '×“×•×’×ž× ×œ×œ× ×§×™×™×', '2021-12-27 15:12:49', '2021-12-27 15:12:49');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` longtext NOT NULL,
  `avatar` varchar(50) DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `otp_code` int(11) DEFAULT NULL,
  `account_confirm_status` varchar(12) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `avatar`, `phone`, `otp_code`, `account_confirm_status`, `created_at`, `updated_at`) VALUES
(1, 'Developer', 'gicehajunior76@gmail.com', '$2y$10$/vzSWWaherBEkeAiH4M.euIBCI4ZUuH4II0ft9sNY5p.07owCqPKq', NULL, NULL, NULL, NULL, '2021-11-28 23:19:26', '2021-11-28 23:19:26'),
(2, 'guy', 'MegiddoGuy@Gmail.com', '$2y$10$KsrdhneeTMqA27HPWKNH2elffsn9acsdq5bkWNZ60xesj9.ChSijq', NULL, NULL, NULL, NULL, '2021-11-29 11:52:46', '2021-11-29 11:52:46'),
(3, 'johnk', 'kanjojohn14@gmail.com', '$2y$10$wujN/vcpZYuANM6VO9XTDewIwKR7TiTvI454W.K41QIzMLgj1438a', NULL, NULL, NULL, NULL, '2021-11-29 16:18:20', '2021-11-29 16:18:20'),
(4, 'nobugcode', 'admin@admin.com', '$2y$10$.HRuD79B1Oyorkrlt5aT8.869pOlhuzr5UzPQ1eBDqnz.9oEzgsPG', NULL, NULL, NULL, NULL, '2021-12-02 09:23:41', '2021-12-02 09:23:41'),
(5, '1', '1', '$2y$10$qo/EJ75H3aAx/1S2Zd1M4.Kj7cfDZqqKqQbntNEK3fl9.1i3aoxcu', NULL, NULL, NULL, NULL, '2022-01-10 13:45:55', '2022-01-10 13:45:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `get_har_data_requests`
--
ALTER TABLE `get_har_data_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `har_bituach_api_settings`
--
ALTER TABLE `har_bituach_api_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partials_settings`
--
ALTER TABLE `partials_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `strings`
--
ALTER TABLE `strings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `get_har_data_requests`
--
ALTER TABLE `get_har_data_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;

--
-- AUTO_INCREMENT for table `har_bituach_api_settings`
--
ALTER TABLE `har_bituach_api_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `partials_settings`
--
ALTER TABLE `partials_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `strings`
--
ALTER TABLE `strings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
